import socket
import chatlib  # To use chatlib functions or consts, use chatlib.****

SERVER_IP = "127.0.0.1"  # Our server will run on same computer as client
SERVER_PORT = 5678

# HELPER SOCKET METHODS

def build_and_send_message(conn, code, data) :
    msg=chatlib.build_message(code, data)
    print(msg)
    conn.sendall(msg.encode())


def recv_message_and_parse(conn):
    """
	Recieves a new message from given socket,
	then parses the message using chatlib.
	Paramaters: conn (socket object)
	    Returns: cmd (str) and data (str) of the received message.
	    If error occured, will return None, None
	    """

    full_msg = conn.recv(1048).decode()

    cmd, data = chatlib.parse_message(full_msg)
    return cmd, data
	
	

def connect():
    client_socket = socket.socket()  # instantiate
    client_socket.connect((SERVER_IP, SERVER_PORT))
    return socket


def error_and_exit(error_msg):
    print(error_msg)
    exit(1)
    pass

def login(conn):
    username = input("Please enter username: \n")
	password = input("Please enter password: \n")

    build_and_send_message(conn, chatlib.PROTOCOL_CLIENT["login_msg"], chatlib.join_data(username,password))
    cmd , data = recv_message_and_parse(conn)
	if cmd == None:
		print("Failed login")
		return	
	else
		print("Logged in!")
		return 
	


def logout(conn):
	build_and_send_message(conn,chatlib.PROTOCOL_CLIENT["logout_msg"],"")	
	

def main():
    # Implement code
    pass

if __name__ == '__main__':
    main()
