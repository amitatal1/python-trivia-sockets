# Protocol Constants
CMD_FIELD_LENGTH = 16
LENGTH_FIELD_LENGTH = 4
MAX_DATA_LENGTH = 10**LENGTH_FIELD_LENGTH - 1
MSG_HEADER_LENGTH = CMD_FIELD_LENGTH + 1 + LENGTH_FIELD_LENGTH + 1
MAX_MSG_LENGTH = MSG_HEADER_LENGTH + MAX_DATA_LENGTH
DELIMITER = "|"
DATA_DELIMITER = "#"
ERROR_RETURN = None

PROTOCOL_CLIENT = {
        "login_msg": "LOGIN",
        "logout_msg": "LOGOUT",
        "connected_users" : "LOGGED",
        "question_req" : "GET_QUESTION",
        "send_answer":"SEND_ANSWER",
        "get_score": "MY_SCORE",
        "highest_score":"HIGHSCORE"
    }

PROTOCOL_SERVER = {
    "login_ok_msg": "LOGIN_OK",
    "login_failed_msg": "ERROR",
    "logged_answer_msg": "LOGGED_ANSWER",
    "your_question_msg": "YOUR_QUESTION",
    "correct_answer_msg": "CORRECT_ANSWER",
    "wrong_answer_msg": "WRONG_ANSWER",
    "your_score_msg": "YOUR_SCORE",
    "all_score_msg": "ALL_SCORE",
    "error_msg": "ERROR",
    "no_questions_msg": "NO_QUESTIONS"
}


def build_message(cmd, data):
    if cmd not in PROTOCOL_CLIENT.values():
        return ERROR_RETURN


    data_length = str(len(data)).zfill(LENGTH_FIELD_LENGTH)
    full_msg = f"{cmd.ljust(CMD_FIELD_LENGTH)}{DELIMITER}{data_length}{DELIMITER}{data}"
    return full_msg


def parse_message(data):
    msg_fields = data.split(DELIMITER)

    if len(msg_fields) != 3:
        return ERROR_RETURN, ERROR_RETURN

    cmd, length, msg_data = msg_fields
    cmd=cmd.strip()
    try:
        expected_length = int(length)
    except ValueError:
        return ERROR_RETURN, ERROR_RETURN

    if len(msg_data) != expected_length or cmd.strip() not in PROTOCOL_CLIENT.values():
        return ERROR_RETURN, ERROR_RETURN

    return cmd, msg_data


def split_data(msg, expected_fields):
    # Implement code to split data using DATA_DELIMITER
    data_fields = msg.split(DATA_DELIMITER)
    
    # Validate the number of fields
    if len(data_fields) != expected_fields:
        return None

    return data_fields


def join_data(msg_fields):
    # Implement code to join data fields using DATA_DELIMITER
    return DATA_DELIMITER.join(msg_fields)


